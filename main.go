package main

import (
	_ "gitee.com/newkkoops/bgadmin/initialize/conf"
	_ "gitee.com/newkkoops/bgadmin/initialize/mysql"
	_ "gitee.com/newkkoops/bgadmin/initialize/session"
	_ "gitee.com/newkkoops/bgadmin/routers"
	_ "gitee.com/newkkoops/bgadmin/utils/template"
	beego "github.com/beego/beego/v2/adapter"
)

func main() {

	//输出文件名和行号
	beego.SetLogFuncCall(true)

	//启动beego
	beego.Run()
}
